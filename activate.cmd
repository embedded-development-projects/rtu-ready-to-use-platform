REM clone the rtu-manifest to our directory
git submodule update --init --recursive

REM import and clone zephyr to out directory
west init


REM config the manifest to be our manifest
west config manifest.path .\rtu-manifest

REM import all of our submodules
west update